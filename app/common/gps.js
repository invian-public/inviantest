const geolocation = require("@nativescript/geolocation");
const { CoreTypes } = require("@nativescript/core");
const ls = require("~/common/ls");
exports.getGeoLocation = function () {
    const promise = new Promise((resolve, reject) => {
        geolocation.enableLocationRequest().
            then(() => {
                geolocation.getCurrentLocation({ desiredAccuracy: CoreTypes.Accuracy.high, maximumAge: 5000, timeout: 20000 }). //any || high
                    then(function (loc) {
                        if (loc) {
                            ls.setNumber("latitude", loc.latitude);
                            ls.setNumber("longitude", loc.longitude);
                            resolve(loc);
                        } else {
                            reject('Error al obtener GPS1')
                        }
                    }, function (e) {
                        console.log(e);
                        reject('Error al obtener GPS2')
                    });
            }, function (e) {
                console.log(e);
                reject('Error al obtener GPS3')
            });
    })
    return promise;
}