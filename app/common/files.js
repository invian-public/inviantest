const fs = require('@nativescript/core/file-system');

exports.getPath = function (platform, is_currentApp, is_db) {
    let folderDest;
    if (is_currentApp) {
        folderDest = fs.knownFolders.currentApp();
    } else {
        folderDest = fs.knownFolders.documents();
    }
    if (platform == 'ios') {
        folderDest = fs.knownFolders.documents();
    } else {
        if (!is_db) {
            // eslint-disable-next-line no-undef
            //const externalPath = fs.path.join(android.os.Environment.getExternalStorageDirectory().getAbsolutePath().toString());
            //folderDest = fs.Folder.fromPath(externalPath + '/ViiAManager');
        }
    }
    folderDest = folderDest.path.replace('/data/data/', '/storage/emulated/0/Android/data/');
    return folderDest;
}
exports.getExistFile = function (path, file) {
    const newpath = path + '/' + file;
    console.log(newpath)
    const exists = fs.File.exists(newpath);
    console.log(`Does ${file} exists: ${exists}`);
    return exists;
}
exports.getExistDataBase = function (path, db) {
    const newpath = path + '/' + db;
    const exists = fs.File.exists(newpath);
    console.log(`Does ${db} exists: ${exists}`);
    return exists;
}
exports.newFolder = function (path, folders) {
    const folderDest = fs.path.join(path, folders);
    const folder = fs.Folder.fromPath(folderDest);
    return folder.path;
}

function setRemoveFile(path, folder, file) {
    const newpath = fs.path.join(path, folder + file);
    const newfile = fs.File.fromPath(newpath);
    return newfile.remove()
        .then(() => {
            console.log("OK delete file: " + newpath);
            return {
                title: 'File successfully deleted!',
                message: newpath
            };
        }).catch((e) => {
            console.log("ERROR deleted file: " + newpath);
            console.log(e.stack);
            return false;
        });
}
exports.removeFile = function (path, folder, file) {
    return setRemoveFile(path, folder, file);
}
async function setRemoveFileMultiple(items) {
    let promises = [];
    items.forEach(async item => {
        promises.push(setRemoveFile(item.path, item.folder, item.file));
    });
    const result = await Promise.all(promises)
    return result;
}
exports.removeFileMultiple = function (items) {
    console.log('removeFileMultiple()');
    console.log(items);
    return setRemoveFileMultiple(items);
}
exports.getFilesFromPath = function (path, extension) {
    console.log(path, extension)
    const promise = new Promise((resolve, reject) => {
        if (path && extension) {
            const folderDest = fs.path.join(path, '');
            const folder = fs.Folder.fromPath(folderDest);
            folder.getEntities()
                .then((entities) => {
                    const newItems = [];
                    entities.forEach((item) => {
                        if (item.extension == extension) {
                            newItems.push({
                                name: item.name,
                                path: item.path,
                                size: item.size,
                                extension: item.extension,
                                lastModified: item.lastModified.toString()
                            });
                        }
                    });
                    resolve(newItems);
                }).catch((e) => {
                    if (e == "TypeError: Cannot read property 'length' of null") {
                        resolve([]);
                    } else {
                        reject(e);
                    }
                });
        } else {
            reject('Extensión: ' + extension + ' referencia: ' + path);
        }
    })
    return promise;
}
exports.removeFilesFromPath = function (path) {
    const folderDest = fs.path.join(path, '');
    const folder = fs.Folder.fromPath(folderDest);
    folder.clear()
        .then(() => {
            console.log("Deleted content folder: ", path);
        }).catch((e) => {
            console.log(e);
        });
}
exports.getSizeFile = function (path) {
    const exists = fs.File.exists(path);
    let size = 0;
    if (exists) {
        const newpath = fs.File.fromPath(path);
        size = newpath.size;
    }
    return size;
}

function setCopyFile(fileorig, pathdest, filename) {
    const promise = new Promise((resolve, reject) => {
        console.log('fileorig', fileorig);
        console.log('pathdest', pathdest);
        console.log('filename', filename);
        const myFile = fs.File.fromPath(fileorig);
        const binarySource = myFile.readSync((e) => {
            console.log(e);
            reject(e);
        });
        const newpathdest = fs.path.join(pathdest, filename);
        const newfile = fs.File.fromPath(newpathdest);
        newfile.write(binarySource)
            .then(() => {
                console.log('File move to:' + pathdest + '/' + filename);
                resolve(pathdest + '/' + filename);
            }).catch((e) => {
                console.log('ERROR writeFileText');
                console.log(e);
                reject(e);
            });
    })
    return promise;
}

exports.copyFile = function (fileorig, pathdest, filename) {
    return setCopyFile(fileorig, pathdest, filename);
}
async function setCopyFileMultiple(items) {
    let promises = [];
    items.forEach(async item => {
        promises.push(setCopyFile(item.fileorig, item.pathdest, item.filename));
    });
    const result = await Promise.all(promises)
    return result;
}
exports.copyFileMultiple = function (items) {
    console.log('copyFileMultiple()');
    console.log(items);
    return setCopyFileMultiple(items);
}
exports.writeFileText = function (path, filename, content) {
    const newpathdest = fs.path.join(path, filename);
    const file = fs.File.fromPath(newpathdest);
    console.log('writeFileText: ', content);
    // file.writeText(content)
    //     .then(() => {}).catch((e) => {
    //         console.log('ERROR writeFileText');
    //         console.log(e);
    //     });
    return file.writeText(content);
}
exports.readFileText = function (path, filename) {
    const newpathdest = fs.path.join(path, filename);
    const file = fs.File.fromPath(newpathdest);
    const promise = file.readText()
        .then((r) => {
            return r;
        }).catch((e) => {
            return (e);
        });
    return promise;
}