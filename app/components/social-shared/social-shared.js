const ViewModel = require('./social-shared-view-model');

const dt = require('~/common/dt');
const SocialShare = require("@nativescript/social-share");

let page;
exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = ViewModel;
    ViewModel.set('value', undefined);
}
exports.tapShared = function() {
    console.log('tapShared()');
    let created_at = dt.formatDateTimeMySql(new Date());
    console.log(created_at);
    created_at = created_at.split(' ');
    SocialShare.shareText(
        '*Compartir:* \n\n' +
        'Texto: ' + ViewModel.get('value') + '\n\n' +
        'Fecha: ' + created_at[0] + '\n' +
        'Hora: ' + created_at[1]
    );
}