const ViewModel = require('./test-view-model');

const ls = require('~/common/ls');
const files = require('~/common/files');
const image = require('~/common/image');

let page;
exports.loaded = function(args) {
    page = args.object;
    ViewModel.set('width', ls.getNumber('screenWidth'));
    ViewModel.set('height', ls.getNumber('screenHeight'));
    page.bindingContext = ViewModel;

    setInit();
}

function setInit() {
    getDevice();
    test_fc()
        .then((r) => {
            console.log(r);
            test_fm()
                .then((r) => {
                    console.log(r);
                    test_fd()
                        .then((r) => {
                            console.log(r);
                        }).catch((e) => {
                            console.log(e);
                        });
                }).catch((e) => {
                    console.log(e);
                });
        }).catch((e) => {
            console.log(e);
        });
}

function getDevice() {
    const current_path = ls.getString('path');
    const compare_path = '/storage/emulated/0/Android/data/org.nativescript.ns8/files/app';
    console.log('current_path', current_path);
    console.log('compare_path', compare_path);
    if (current_path == compare_path) {
        ViewModel.set('d_icon', '\uf10d');
        ViewModel.set('d_icon_color', 'green');
        ViewModel.set('d_title', 'OK path');
        ViewModel.set('d_response', current_path);
        ViewModel.set('d_time', '');
    } else {
        ViewModel.set('d_icon', '\uf102');
        ViewModel.set('d_icon_color', 'red');
        ViewModel.set('d_title', 'ERROR path extraño');
        ViewModel.set('d_response', current_path);
        ViewModel.set('d_time', '');
    }
}

//TEST creación de folders y archivos
function setFolders() {
    const promise = new Promise((resolve, reject) => {
        try {
            const path = files.getPath(ls.getString('platform'), true, false);
            ls.setString('path', path);
            const path_queue = files.newFolder(path, 'queue');
            const path_tmp = files.newFolder(path, 'tmp');
            const path_backup = files.newFolder(path, 'backup');
            ls.setString('path_queue', path_queue);
            ls.setString('path_tmp', path_tmp);
            ls.setString('path_backup', path_backup);
            resolve(true);
        } catch (e) {
            reject(e);
        }
    })
    return promise;
}

function test_fc() {
    console.log('test_fc()');
    const promise = new Promise((resolve, reject) => {
        ViewModel.set('fc_icon', '\uf106');
        ViewModel.set('fc_icon_color', 'yellow');
        ViewModel.set('fc_title', 'Inició crear');
        ViewModel.set('fc_response', '');
        ViewModel.set('fc_time', '');
        const start = Date.now();
        setFolders()
            .then((r) => {
                if (r) {
                    files.writeFileText(ls.getString('path_backup'), 'test_fc.txt', 'Hello wrold')
                        .then((r) => {
                            ViewModel.set('fc_icon', '\uf10d');
                            ViewModel.set('fc_icon_color', 'green');
                            ViewModel.set('fc_title', 'OK escribir archivo: ', r);
                            ViewModel.set('fc_response', r);
                            const end = Date.now();
                            ViewModel.set('fc_time', end - start);
                            resolve(true);
                        }).catch((e) => {
                            ViewModel.set('fc_icon', '\uf102');
                            ViewModel.set('fc_icon_color', 'red');
                            ViewModel.set('fc_title', 'ERROR escribir archivo: ' + e);
                            ViewModel.set('fc_response', e);
                            const end = Date.now();
                            ViewModel.set('fc_time', end - start);
                            reject(e);
                        });
                }
            }).catch((e) => {
                ViewModel.set('fc_icon', '\uf102');
                ViewModel.set('fc_icon_color', 'red');
                ViewModel.set('fc_title', 'ERROR crear folder: ' + e);
                ViewModel.set('fc_response', e);
                const end = Date.now();
                ViewModel.set('fc_time', end - start);
                reject(e);
            });
    })
    return promise;
}

//TEST mover archivos: de backup al tmp
function test_fm() {
    console.log('test_fm()');
    const promise = new Promise((resolve, reject) => {
        ViewModel.set('fm_icon', '\uf106');
        ViewModel.set('fm_icon_color', 'yellow');
        ViewModel.set('fm_title', 'Inició mover');
        ViewModel.set('fm_response', '');
        ViewModel.set('fm_time', '');
        const start = Date.now();
        files.copyFile(ls.getString('path_backup') + '/' + 'test_fc.txt', ls.getString('path_tmp'), 'test_fm.txt')
            .then((r) => {
                ViewModel.set('fm_icon', '\uf10d');
                ViewModel.set('fm_icon_color', 'green');
                ViewModel.set('fm_title', 'OK mover archivo: ', r);
                ViewModel.set('fm_response', r);
                const end = Date.now();
                ViewModel.set('fm_time', end - start);
                resolve(true);
            }).catch((e) => {
                ViewModel.set('fm_icon', '\uf102');
                ViewModel.set('fm_icon_color', 'red');
                ViewModel.set('fm_title', 'ERROR mover archivo: ' + e);
                ViewModel.set('fm_response', e);
                const end = Date.now();
                ViewModel.set('fm_time', end - start);
                reject(e);
            });
    })
    return promise;
}

//TEST eliminar archivos
function test_fd() {
    console.log('test_fd()');

    const promise = new Promise((resolve, reject) => {
        ViewModel.set('fd_icon', '\uf106');
        ViewModel.set('fd_icon_color', 'yellow');
        ViewModel.set('fd_title', 'Inició eliminar');
        ViewModel.set('fd_response', '');
        ViewModel.set('fd_time', '');
        const start = Date.now();
        files.copyFile(ls.getString('path_backup') + '/' + 'test_fc.txt', ls.getString('path_tmp'), 'test_fm.txt')
            .then((r) => {
                ViewModel.set('fd_icon', '\uf10d');
                ViewModel.set('fd_icon_color', 'green');
                ViewModel.set('fd_title', 'OK mover archivo: ', r);
                ViewModel.set('fd_response', r);
                const end = Date.now();
                ViewModel.set('fd_time', end - start);
                resolve(true);
            }).catch((e) => {
                ViewModel.set('fd_icon', '\uf102');
                ViewModel.set('fd_icon_color', 'red');
                ViewModel.set('fd_title', 'ERROR mover archivo: ' + e);
                ViewModel.set('fd_response', e);
                const end = Date.now();
                ViewModel.set('fd_time', end - start);
                reject(e);
            });
    })
    return promise;
}



//CAMERA
exports.tapAdd = function() {
    console.log('tapAdd()');
    const image_name = Date.now();
    image.takePicture(ls.getString('path_tmp'), ls.getString('platform'), image_name).
    then((r) => {
        console.log(r);
        if (r) {
            const item = [{
                name: image_name,
                src: r
            }]
            setContent(item);
        }
    }, (e) => {
        console.log(e);
    });
}

function setContent(item) {
    console.log('setContent()');
    page.getViewById('name').text = item[0].name;
    page.getViewById('path').text = item[0].src;
    page.getViewById('img').src = item[0].src;
}