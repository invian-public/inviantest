const ViewModel = require("./details-view-model");

const ls = require('~/common/ls');
const nav = require('~/common/nav');

const SocialShare = require("@nativescript/social-share");

let page;
exports.navigatingTo = function(args) {
    page = args.object;
    const item = page.navigationContext;
    ViewModel.set('image_url_xl', undefined);
    for (const i in item) {
        ViewModel.set(i, item[i]);
    }
    ViewModel.set('width', ls.getNumber('screenWidth'));
    page.bindingContext = ViewModel;
}
exports.tapBack = function() {
    nav.goBack(page.page)
}

function setImageXL() {
    ViewModel.set('image_url_xl', ls.imgSize(ViewModel.get('image_url'), '_xl'));
}
exports.refreshSizeImage = function(args) {
    const img = args.object;
    if (ViewModel.get('image_url') && img.isLoaded) {
        setTimeout(() => {
            setImageXL();
        }, 500);
    }
}
exports.tapShared = function() {
    let updated_at = ViewModel.get('updated_at');
    updated_at = updated_at.split(' ');
    SocialShare.shareText(
        '*' + ViewModel.get('footer') + '* \n\n' +
        'Servicio: ViiA PoP \n' +
        'Cara: ' + ViewModel.get('title') + ' (' + ViewModel.get('subtitle') + ')\n' +
        'Arte: ' + ViewModel.get('description') + '\n' +
        'Fecha: ' + updated_at[0] + '\n' +
        'Hora: ' + updated_at[1] + '\n\n' +
        ls.imgSize(ViewModel.get('image_url'), '')
    );
}