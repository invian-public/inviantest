const ViewModel = require("./nstudio-view-model");

const ls = require("~/common/ls");
const { VideoEditor } = require("@viia/nativescript-video-editor");

let page;
exports.loaded = function (args) {
  page = args.object;
  page.bindingContext = ViewModel;
  ViewModel.set("onSuccess", onSuccess);
  ViewModel.set("repeater", []);
};

exports.trimmerLoaded = function (args) {
  const view = args.object;
  const parent = view.parent;
//   const start = parent.getViewById("start");
//   const end = parent.getViewById("end");
//   const size = parent.getViewById("size");
};

exports.infoChanged = function (args) {
  const view = args.object;
  const parent = view.parent;
  const size = parent.getViewById("size");
  size.text = args.info;
};

exports.onSelectRange = function (args) {
    const view = args.object;
    const parent = view.parent;
    const start = parent.getViewById("start");
    start.text = args.start;
    const end = parent.getViewById("end");
    end.text = args.end;
};

exports.onSelectRangeEnd = function (args) {
    const view = args.object;
    const parent = view.parent;
    const start = parent.getViewById("start");
    start.text = args.start;
    const end = parent.getViewById("end");
    end.text = args.end;
};

exports.cloneVideo = function (args) {
  const view = args.object;
  const parent = view.parent;
  const trimmer = parent.getViewById("trimmer");
  trimmer.trim();
};

const Color = require("@nativescript/core/color").Color;

VideoEditor.config.edit.title = "Title111";
VideoEditor.config.edit.titleColor = "#ffffff";
VideoEditor.config.edit.titleBarColor = "#00c875";
VideoEditor.config.edit.barBackgroundColor = "#111111";
VideoEditor.config.edit.barForegroundColor = "#555555";
VideoEditor.config.edit.barBackgroundColor = "#666666";
VideoEditor.config.edit.barForegroundColor = "#00c875";
VideoEditor.config.edit.barOverLayColor = "#ffcb00";
VideoEditor.config.edit.borderColor = "#00c875";
VideoEditor.config.edit.doneButtonColor = "#3d9be9";
VideoEditor.config.edit.cloneButtonTitle = "CLONE";
VideoEditor.config.edit.videoIndicatorColor = "#00c875";

VideoEditor.config.camera.pickerButtonColor = new Color("#00c875");
VideoEditor.config.camera.doneButtonColor = new Color("#00c875");
VideoEditor.config.camera.flashButtonOnColor = new Color("#00c875");
VideoEditor.config.camera.flashButtonOffColor = new Color("#00c875");
VideoEditor.config.camera.modeButtonOnColor = new Color("#00c875");
VideoEditor.config.camera.modeButtonOffColor = new Color("#00c875");
VideoEditor.config.camera.cancelNoColor = new Color("#00c875");
VideoEditor.config.camera.cancelOkColor = new Color("#00c875");
//The following can be use to update the cancel dialog it is only show if the user has taken or imported file(s)
VideoEditor.config.camera.cancelTitle = "Cancelar"; // title defaults to Cancel
VideoEditor.config.camera.cancelMessage =
  "Todos los archivos capturados serán eliminados, ¿estás seguro?"; // message defaults to "All files captured will be removed, are you sure?";
VideoEditor.config.camera.cancelOk = "Sí"; // ok defaults to Yes
VideoEditor.config.camera.cancelNo = "No"; // cancel defaults to No

VideoEditor.config.confirm.title = "title1";
VideoEditor.config.confirm.titleColor = new Color("#e2445c");
VideoEditor.config.confirm.titleBarColor = new Color("#00c875");
VideoEditor.config.confirm.doneButtonColor = new Color("#ffcb00");
VideoEditor.config.confirm.cancelButtonColor = new Color("#3d9be9");
VideoEditor.config.confirm.doneButtonTitle = "title2";

exports.openCamera = function (args) {
  const btn = args.object;
  btn.isEnabled = false;

  const config = {
    saveToGallery: false,
    maxPhotoCount: 3,
    maxVideoCount: 3,
    mode: "VIDEO", //'PHOTO' | 'VIDEO' | 'BOTH';
    pictureSize: "800x600", // will try to use the closest supported size
    ratio: "4:3", //'4:3,16:9' the underlaying camerax supports these only
    filePath: ls.getString("path_tmp"),
    //fileName: 'TEST',
    enableAudio: false,
    videoQuality: "HIGHEST",
    enableAttach: true, // hide the attach button
    showVideoEditor: false, // hide video timing functionality
  };
  console.log(config);
  VideoEditor.openCamera(config)
    .then((r) => {
      btn.isEnabled = true;
      console.log(r);
      setContent(r);
    })
    .catch((e) => {
      btn.isEnabled = true;
      console.log(e);
    });
};

function getCapturedAt(name) {
  name = name.replace("VID_", "").replace(".mp4", "");
  name = name.replace("IMG_", "").replace(".jpg", "");

  const y = name.substring(0, 4);
  const m = name.substring(4, 6);
  const d = name.substring(6, 8);
  const hour = name.substring(8, 10);
  const min = name.substring(10, 12);
  const seg = name.substring(12, 14);

  return y + "-" + m + "-" + d + " " + hour + ":" + min + ":" + seg;
}

function msegToSeg(m) {
  return parseInt(m / 1000) + " seg";
}

function setContent(items) {
  const repeater = ViewModel.get("repeater");
  //console.log('repeater', repeater);
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    repeater.push(
      ViewModel.observer({
        index: repeater.length,
        id: "video" + (repeater.length + 1),
        name: "Video " + (repeater.length + 1),
        src: item.file,
        captured_at: getCapturedAt(item.name),
        size: item.displaySize,
        size_items: item.sizeInBytes,
        seconds: item.duration,
        duration: msegToSeg(item.duration),
        rotate: 90,
        play: false,
        campaing: undefined,
        art: undefined,
      })
    );
  }
  //console.log('repeater', repeater);
  page.getViewById("repeater").refresh();
}

function onSuccess(event) {
  const item = event.result;
  const repeater = ViewModel.get("repeater") || [];
  repeater.push(
    ViewModel.observer({
      index: repeater.length,
      id: "video" + (repeater.length + 1),
      name: "Video " + (repeater.length + 1),
      src: item.file,
      captured_at: getCapturedAt(item.name),
      size: item.displaySize,
      size_items: item.sizeInBytes,
      seconds: item.duration,
      duration: msegToSeg(item.duration),
      rotate: 90,
      play: false,
      campaing: undefined,
      art: undefined,
    })
  );
 // ViewModel.set("repeater", repeater);
// page.getViewById("repeater").items = repeater;
  page.getViewById("repeater").refresh();
}

exports.testIt = function (args) {
  const btn = args.object;
  btn.isEnabled = false;
  const path =
    "/storage/emulated/0/Android/data/org.nativescript.ns8/files/VIDEO_EDITOR/VID_20220614231219.mp4";
  VideoEditor.openVideoEditor(path)
    .then((r) => {
      btn.isEnabled = true;
      console.log(r);
    })
    .catch((e) => {
      btn.isEnabled = true;
      console.log(e);
    });
};
