const ViewModel = require("./image-picker-view-model");

const ls = require("~/common/ls");
const dt = require("~/common/dt");
const imagepicker = require("@nativescript/imagepicker");
const { ImagePickerMediaType } = require("@nativescript/imagepicker");
const { ImageAsset, ImageSource } = require("@nativescript/core");

let page;
exports.loaded = function (args) {
  page = args.object;
  ViewModel.set("width", ls.getNumber("screenWidth"));
  ViewModel.set("height", ls.getNumber("screenHeight"));
  page.bindingContext = ViewModel;
};

//MODAL CAMERA LOGIC
exports.tapAdd = function (args) {
  const list = [];
  const type = args.object.type;
  let mediaType;
  if (type == "video") {
    mediaType = ImagePickerMediaType.Video;
  } else {
    mediaType = ImagePickerMediaType.Image;
  }

  const context = imagepicker.create({
    mode: "multiple",
    mediaType: mediaType,
  }); // use 'multiple' or 'single' for multiple selection // Image/Video/Any
  context
    .authorize()
    .then(function () {
      return context.present();
    })
    .then(function (selection) {
      selection.forEach(function (fileAsset) {
        list.push({
          src: fileAsset,
          captured_at: dt.formatDateTimeMySql(new Date()),
          duration: 0,
          rotate: 0,
        });
      });
      setContent(list);
    })
    .catch(function (error) {
      console.log(err);
    });
};

function setContent(list) {
  console.log("setContent()");
  console.log(list);
  const first = list[0];
  const type = first?.src?.type;
  const path = first?.src?.path;
  page.getViewById("name").text = first.name;
  page.getViewById("type").text = type;
  page.getViewById("captured_at").text = first.captured_at;
  page.getViewById("path").text = path;

  if (type == "video") {
    console.log(first.src);
    page.getViewById("video").src = path;
    page.getViewById("image").visibility = "collapsed";
    page.getViewById("video").visibility = "visible";
  } else {
    const imageView = page.getViewById("image");
    const src = first.src;
    if (src.asset instanceof ImageAsset) {
      ImageSource.fromAsset(src.asset).then((source) => {
        imageView.imageSource = source;
      });
    } else if (src instanceof ImageSource) {
      imageView.imageSource = src;
    } else {
      imageView.src = src;
    }
    page.getViewById("video").visibility = "collapsed";
    imageView.visibility = "visible";
  }
}
