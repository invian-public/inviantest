const ViewModel = require('./camera-plus-view-model');

const ls = require('~/common/ls');
const modalcameramulti = "~/modal/cameramulti/cameramulti";

let page;
exports.loaded = function(args) {
    page = args.object;
    ViewModel.set('width', ls.getNumber('screenWidth'));
    ViewModel.set('height', ls.getNumber('screenHeight'));
    page.bindingContext = ViewModel;
}

//MODAL CAMERA LOGIC
let camIsOpen = false;
exports.tapAdd = function(args) {
    if (camIsOpen == false) {
        camIsOpen = true;
        const item = args.object.bindingContext;
        const type = args.object.type;
        setModalCamera(getModalCamera(type), item.index);
    }
}

function getModalCamera(type) {
    let item;
    if (type == 'video') {
        item = {
            isImage: false,
            value: null,
            total: 0,
            loaded: false,
            sdk: ls.getNumber('sdk'),
            folderPath: ls.getString('path_tmp') + '/tmp',
            recording: false,
            duration: 0
        }
    } else { // image
        item = {
            isImage: true,
            value: null,
            total: 0,
            loaded: false,
            sdk: ls.getNumber('sdk'),
            folderPath: ls.getString('path_tmp') + '/tmp'
        };
    }
    return item;
}

function setModalCamera(params, index) {
    params.index = index;
    const platform = ls.getString('platform');
    const option = {
        context: {
            params: params,
            platform: platform
        },
        closeCallback: (item, index) => {
            camIsOpen = false;
            if (item) {
                setContent(item, index);
            }
        },
        fullscreen: true
    };
    if (platform == 'ios') {
        option.ios = {
            // eslint-disable-next-line no-undef
            presentationStyle: UIModalPresentationStyle.OverFullScreen
        }
    }
    page.showModal(modalcameramulti, option);
}

function setContent(item) {
    console.log('setContent()');
    console.log(item);
    page.getViewById('name').text = item[0].name;
    page.getViewById('type').text = item[0].type;
    page.getViewById('captured_at').text = item[0].captured_at;
    page.getViewById('path').text = item[0].src;
    if (item[0].type == 'video') {
        console.log(page.getViewById('video'));
        page.getViewById('video').src = item[0].src;
        page.getViewById('image').visibility = 'collapsed';
        page.getViewById('video').visibility = 'visible';
    } else {
        page.getViewById('image').src = item[0].src;
        page.getViewById('video').visibility = 'collapsed';
        page.getViewById('image').visibility = 'visible';
    }
}