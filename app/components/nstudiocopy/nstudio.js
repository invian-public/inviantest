const ViewModel = require('./nstudio-view-model');

const ls = require('~/common/ls');
const image = require('~/common/image');
const files = require('~/common/files');

const { VideoEditor } = require("@viia/nativescript-video-editor");

let page;
exports.loaded = function (args) {
    page = args.object;
    ViewModel.set('width', ls.getNumber('screenWidth'));
    ViewModel.set('height', ls.getNumber('screenHeight'));
    page.bindingContext = ViewModel;
}

exports.tapAdd = function () {
    console.log('tapAdd()');
    const image_name = Date.now();
    image.takePicture(ls.getString('path_tmp'), ls.getString('platform'), image_name).
        then((r) => {
            console.log(r);
            if (r) {
                const item = [{
                    name: image_name + '.jpg',
                    file: r
                }]
                setContent(item[0]);
            }
        }, (e) => {
            console.log(e);
        });
}

function setContent(item) {
    console.log('setContent()');
    console.log(item);
    page.getViewById('name').text = item.name;
    page.getViewById('path').text = item.file;
    page.getViewById('img1').src = item.file;
    page.getViewById('btn').visibility = 'visible';
}

exports.tapMove = function () {
    console.log('tapMove()');
    const name = page.getViewById('name').text;
    const path = page.getViewById('path').text;
    const pathdest = ls.getString('path_backup');
    files.copyFile(path, pathdest, name).
        then((r) => {
            page.getViewById('img2').src = (r);
        }, (e) => {
            console.log(e);
        });
}

//NSTUDIO CAMERA
exports.tapAddNstudio = function (args) {
    const config = {
        saveToGallery: false,
        maxPhotoCount: 3,
        maxVideoCount: 3,
        mode: "PHOTO", //'PHOTO' | 'VIDEO' | 'BOTH';
        pictureSize: "800x600", // will try to use the closest supported size
        ratio: "4:3", //'4:3,16:9' the underlaying camerax supports these only
        filePath: ls.getString("path_tmp"),
        //fileName: 'TEST',
        enableAudio: false,
        videoQuality: "HIGHEST",
        enableAttach: true, // hide the attach button
        showVideoEditor: false, // hide video timing functionality
    };
    VideoEditor.openCamera(config)
        .then((r) => {
            console.log(r);
            setContent(r[0]);
        })
        .catch((e) => {
            console.log(e);
        });
};